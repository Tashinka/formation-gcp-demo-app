# Formation GCP - Demo app

A simple Flask app.

## Usage

1. Clone the project:
   ```bash
   git clone https://gitlab.com/Tashinka/formation-gcp-demo-app.git
   cd formation-gcp-demo-app
   ```

2. Build the image:
   ```bash
   docker build . \
       -t <region>-docker.pkg.dev/<project_id>/<registry_name>/<image_name>:<image_tag>
   ```

3. Push the image:
   ```bash
   docker push <region>-docker.pkg.dev/<project_id>/<registry_name>/<image_name>
   ```
