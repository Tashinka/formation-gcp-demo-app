FROM python:3.10-alpine

ENV PORT 80 

WORKDIR /app

COPY ./app /app

RUN pip3 install -r /app/requirements.txt

ENTRYPOINT ["python3"]
CMD ["main.py"]

EXPOSE $PORT
